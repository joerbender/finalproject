//Represents the internal state of a square
class SquareState {
    
    constructor(id, rowIndex, colIndex, handleSquareExposed) {
        this.id = id;
        this.__row = rowIndex;
        this.__column = colIndex;
        this.__isMine = false;
        this.__isExposed = false;
        this.__adjacentSquareStates = [];
        this.__flagState = 0;
        this.__isExplodedMine = false;
        this.__isEnabled = true;

        // propagate the input function to this instance
        this.__handleSquareExposed = handleSquareExposed;

        this.setMine = this.setMine.bind(this);
        this.isEnabled = this.isEnabled.bind(this);
        this.setEnabled = this.setEnabled.bind(this);
        this.getFlagState = this.getFlagState.bind(this);
        this.incrementFlagState = this.incrementFlagState.bind(this);
        this.isExposed = this.isExposed.bind(this);
        this.getAdjacentMineCount = this.getAdjacentMineCount.bind(this);
        this.appendAdjacentSquareState = this.appendAdjacentSquareState.bind(this);
        this.getColumn = this.getColumn.bind(this);
        this.getRow = this.getRow.bind(this);
    }

    // checks if the square is enabled for clicking
    isEnabled() {
        return this.__isEnabled;
    }
    
    // sets the square as enabled/disabled
    setAsEnabled(isEnabled) {
        this.__isEnabled = isEnabled;
    }

    // gets the square column
    getColumn() {
        return this.__column;
    }

    // gets the square row
    getRow() {
        return this.__row;
    }

    // checks if this was the mine that was exposed
    isEndGameMine() {
        return this.__isEndGameMine;
    }

    // checks if this is a mine
    isMine() {
        return this.__isMine;
    }

    // gets the number of mines that this state is adjacent to
    getAdjacentMineCount() {
        return this.__isMine ? 0 : this.__adjacentSquareStates.filter(adj => adj.isMine()).length;
    }

    // exposes the square state
    expose() {
        if (!this.__isExposed) {
            this.__isExposed = true;
            if (this.__isMine) {
                this.__isExplodedMine = true;
            }
            
            // to speed up the game if the square is not mine adjacent then expose all its adjacent squares (recursively)
            if (!this.__isMine && this.getAdjacentMineCount() === 0) {
                this.__adjacentSquareStates.forEach(adjacentSquareState => adjacentSquareState.expose());
            }
            this.__handleSquareExposed();
        }
    }

    // appends an adjacent square for this square to reference
    appendAdjacentSquareState(adjSquareState) {
        this.__adjacentSquareStates.push(adjSquareState);
    }

    // checks whether the state is exposed 
    isExposed() {
        return this.__isExposed;
    }

    // gets the state of the flag
    getFlagState() {
        return this.__flagState;
    }

    // Increments the state of the flag
    incrementFlagState() {
        this.__flagState = (this.__flagState + 1) % 3;
        return this.__flagState;
    }

    // Sets this mine to the desired value
    setMine(isMine) {
        this.__isMine = isMine;
    }
    
    // Sets this mine to the desired value
    setEnabled(isEnabled) {
        this.__isEnabled = isEnabled;
    }
}

export default SquareState;