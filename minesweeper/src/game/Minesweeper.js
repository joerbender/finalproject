import React, { Component } from 'react';
import Board from './Board';
import BoardState from './BoardState';
import styles from './game.module.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import { withRouter } from 'react-router-dom';
import { Table, Grid } from '@material-ui/core';
import Axios from 'axios';

const sad = "sad.png";
const bored = "bored.png";
const happy = "happy.png";
const infinityChar = '\u221e';

//Represents the gameplay of the minesweeper app
class Minesweeper extends Component {

  constructor(props) {
    super(props)

    // grab the input board dimensions and mine specs
    const { width, height, numMines} = props;

    this.state = {
      boardState: new BoardState(width, height, numMines),
      elapsedTime: infinityChar,
      numRemainingMines: numMines,
      startTime: 0,
      timer: false,                  
      faceImage: happy
    }

    this.handleSmileyClicked = this.handleSmileyClicked.bind(this);
    this.restartGame = this.resetGame.bind(this);
    this.timerAction = this.timerAction.bind(this);
    this.setElapsed = this.updateElapsedTime.bind(this);
    this.handleGameOver = this.handleGameOver.bind(this);
    this.handleGameStarted = this.handleGameStarted.bind(this)
    this.handleSquareFlagged = this.handleSquareFlagged.bind(this);
}


// handles the operations associated with the game ending, either win or lose
  async handleGameOver(isWon) {
    this.setState({ faceImage: isWon ? happy : sad, timer: false });
    if (isWon) {
      try {
        const { elapsedTime } = this.state;
        const { signedInUser, width, height, numMines } = this.props;
        //post the score to the database
        await Axios.post('/api/score', {
          score: elapsedTime,
          user: signedInUser,
          boardWidth: width,
          boardHeight: height,
          mineCount: numMines
        });
      } catch (error) {
        console.log(error);
      }
    } else {
      this.setState({ faceImage: sad });
    }
    this.setState({ timer: false });
  }

  // handles the operations of when a square is flagged (decrements the mine count)
  handleSquareFlagged(flag) {
    if (flag > 0) {
      const { numRemainingMines } = this.state;
      this.setState({ numRemainingMines: numRemainingMines - ((flag === 1) ? 1 : -1) });
    }
  }

  // handles the operations of when the game is started (basically starts the clock)
  handleGameStarted() {
    const { timer } = this.state;
    if (!timer) {
      this.setState({ startTime: new Date(), timer: true }, this.timerAction);
    }
  }

  // updates the time to be displayed to the user
  updateElapsedTime() {
    const { startTime, timer } = this.state;
    if (timer && startTime) {
      const now = new Date();
      const secs = Math.floor((now.getTime() - startTime.getTime()) / 1000);
      this.setState({ elapsedTime: (secs > 999 ? infinityChar : "" + secs) });
    } else {
      this.setState({ elapsedTime: infinityChar });
    }
  }

  // acts as a 1 second continuous timer
  timerAction() {
    const { timer } = this.state;
    if (timer) {
      this.updateElapsedTime();
      setTimeout(this.timerAction, 100);
    }
  }

  // resets the game to an intial state
  resetGame() {
    const { width, height, numMines } = this.props;
    this.setState({ boardState: new BoardState(width, height, numMines), 
                    faceImage: bored, 
                    timer: false,
                    numRemainingMines: numMines }, this.updateElapsedTime);
  }

  // handles when the smiley face is clicked
  handleSmileyClicked(event) {
    if (!event) event = window.event;
    if (event.button !== 2) {
      this.resetGame();
    }
  }

  //renders the full game on the screen
  render() {
    const { numRemainingMines, 
            elapsedTime,
            faceImage,
            boardState } = this.state;
    return (
      <div>
        <CssBaseline/>
        <Grid container className={styles.score} direction="row" justify="space-evenly">
            <Grid item className={styles.counter} align="center" id={styles.mines}>{numRemainingMines}</Grid>
            <Grid item align="center">
              <img id={styles.smiley} src={require("./image/" + faceImage)} alt="" onMouseDown={this.handleSmileyClicked} />
            </Grid>
            <Grid item align="center" className={styles.counter} id={styles.timer}>{elapsedTime}</Grid>
          </Grid>
          <Table>
            <Board boardState={boardState}
              handleGameOver={this.handleGameOver}
              handleGameStarted={this.handleGameStarted}
              handleSquareFlagged={this.handleSquareFlagged} />
          </Table>
      </div>
    );
  }
}

export default withRouter(Minesweeper);
