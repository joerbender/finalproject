import React, { Component } from 'react';
import Square from './Square';
import { TableRow, TableBody } from '@material-ui/core';

//represents the displayed board of the mine sweeper game
class Board extends Component {

    constructor(props) {
        super(props);

        this.renderRow = this.renderRow.bind(this);
        this.handleSquareExposed = this.handleSquareExposed.bind(this);
    }

    // handles when a square is exposed by the user
    handleSquareExposed(squareState) {
        const { handleGameOver, handleGameStarted, boardState } = this.props;
        if (!boardState.isGameStarted()) {
            handleGameStarted(squareState);
            boardState.generateMines(squareState);
        } else if (squareState.isMine()) {
            handleGameOver(false);
            boardState.disableSquares();
            this.setState({});
        } else if (boardState.isGameWon()) {            
            handleGameOver(true);
        } else if (squareState.getAdjacentMineCount() === 0) {
            this.setState({});
        }
    }

    // renders an entire row of squares given their square states
    renderRow(squareRow, index) {
        const { handleSquareFlagged } = this.props;
        return <TableRow key={"row" + index}>
            {squareRow.map(squareState => 
                <Square key={"sq" + squareState.id}
                    squareState={squareState}
                    handleSquareFlagged={handleSquareFlagged}
                    handleSquareExposed={this.handleSquareExposed}/>)}
        </TableRow>
    }

    //renders the board
    render() {
        const { boardState } = this.props;
        return <TableBody>
            {boardState.squareRows.map((row, index) => this.renderRow(row, index))}
        </TableBody>;
    }
}

export default Board;