import SquareState from "./SquareState";

//the relative translations of each adjcency of a given square
const adjacencyMatrix = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];

// Used to handle the state of the minesweeper board
class BoardState {

    constructor(width, height, numMines) {
        this.width = width;
        this.height = height;
        this.numMines = numMines;
        this.__numExposed = 0;
        this.__gameStarted = false;

        this.generateBoardState = this.generateBoardState.bind(this);
        this.generateMines = this.generateMines.bind(this);
        this.setMineAdjacencies = this.setAdjacencies.bind(this);
        this.handleSquareExposed = this.handleSquareExposed.bind(this);
        this.isGameStarted = this.isGameStarted.bind(this);
        this.isGameWon = this.isGameWon.bind(this);
        this.disableSquares = this.disableSquares.bind(this);
        
        this.squareRows = this.generateBoardState();
    }

    //disables all squares on the baord so the user cannot click
    disableSquares() {
        this.squareRows.forEach(squareRow => squareRow.forEach(squareState => squareState.setEnabled(false)));
    }

    // checks if the game has been started
    isGameStarted() {
       return this.__gameStarted;
    }

    // checks if the game is over
    isGameWon() {
        //check if all safe squares have been exposed
        return this.__numExposed + 1 >= this.width * this.height - this.numMines;
    }

    // handles when a square is clicked/exposed
    handleSquareExposed() {
        this.__numExposed++;
    }

    // generated the mines on the board, the input safe square should be the first square selected by the user
    // this safe square will never be a mine and makes sure that the user always starts the game safely 
    generateMines(safeSquare) {
        const mines = new Set();
        while(mines.size < this.numMines) {
            const mineRow = Math.floor(Math.random() * this.width);
            const mineColumn = Math.floor(Math.random() * this.height);
            if (mineRow !== safeSquare.getRow() || mineColumn !== safeSquare.getColumn()) {
                mines.add([mineRow, mineColumn]);
            }
        }
        mines.forEach(mine => this.squareRows[mine[0]][mine[1]].setMine(true));
        this.__gameStarted = true;
    }

    // populates the adjacencies of the the square state with its adjacent square states
    setAdjacencies(squareState, squareRows) {
        const row = squareState.getRow();
        const col = squareState.getColumn();
        adjacencyMatrix.map(adjTrans => [row + adjTrans[0], col + adjTrans[1]])
                        .filter(adj => adj.filter(val => val >= 0 && val < this.width).length === 2)
                        .map(adj => squareRows[adj[0]][adj[1]])
                        .forEach(adjState => squareState.appendAdjacentSquareState(adjState));
    }

    // generates the initial board state (without any mines)
    generateBoardState() {
        const squareRows = Array.from(Array(this.width).keys())
                        .map(columnIndex => Array.from(Array(this.height).keys())
                            .map(rowIndex => {
                                return new SquareState(`${this.width}x${this.height}@${columnIndex}_${rowIndex}`,
                                    columnIndex,
                                    rowIndex,
                                    this.handleSquareExposed);
                            }));
        squareRows.forEach(squareRow => squareRow.forEach(squareState => this.setAdjacencies(squareState, squareRows)));
        return squareRows;
    }
}

export default BoardState;