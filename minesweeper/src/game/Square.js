import React, { Component } from 'react';
import styles from './game.module.css';
import { TableCell } from '@material-ui/core';

// define the adjacent square styles from the css module
const numStyles = [styles.sq1, 
                   styles.sq2, 
                   styles.sq3, 
                   styles.sq4, 
                   styles.sq5, 
                   styles.sq6, 
                   styles.sq7,
                   styles.sq8]

//represents the rendered square on the game board
class Square extends Component {

    constructor(props) {
        super(props);

        this.handleClicked = this.handleClicked.bind(this);
        this.getDisplayState = this.getDisplayState.bind(this);
    }

    // handles when the suqare is clicked
    handleClicked(event) {
        const { squareState, handleSquareExposed, handleSquareFlagged } = this.props;
        if (squareState.isEnabled()) {
            if (!squareState.isExposed()) {
                if (event.button === 0) {
                    // dont update if a flag has been set
                    if (squareState.getFlagState() !== 1) {
                        handleSquareExposed(squareState);
                        squareState.expose();
                        this.setState({squareState: squareState});
                    }
                } else {
                    handleSquareFlagged(squareState.incrementFlagState());
                    this.setState({squareState: squareState});
                }
            }
        }
        return false;
    }

    // gets the display style of the square based on the squares state
    getDisplayState() {
        const { squareState } = this.props;
        const sq = `${styles.sq}`;
        const flagState = squareState.getFlagState();
        if (squareState.isMine()) {
            if (!squareState.isEnabled()) {
                if (flagState === 1) {
                    //wrong! you stupid idiot
                    return {
                        // show a red asterix
                        className: `${sq} ${styles.sqIncorrect}`,
                        value: "*"
                    };
                }
                return {
                    // show a red bomb
                    className: `${sq} ${squareState.isEndGameMine() ? styles.sqExploded : ""}`,
                    value: "X"
                }
            } 
        }
        if (squareState.isExposed()) {
            const adjMines = squareState.getAdjacentMineCount();
            const isMineAdjacent = adjMines > 0;
            return {
                //show the adjacent mine count
                className: `${sq} ${styles.sqExposed} ${(isMineAdjacent ? numStyles[adjMines - 1] : "")}`,
                value: `${(isMineAdjacent ? adjMines : " ")}`
            };
        }        
        if (flagState === 1) {
            return {
                //show an exclamation mark
                className: `${sq} ${styles.sqFlagged}`,
                value: '!'
            };
        }
        if (flagState === 2) {
            return {
                //show a question mark
                className: `${sq} ${styles.sqQuestion}`,
                value: '?'
            }
        }
        return {
            // just show the basic unexposed square
            className: `${sq}`,
            value: ' '
        };
    }

    // render the square
    render() {
        const { isGameOver } = this.props;
        const displayState = this.getDisplayState(isGameOver);
        return <TableCell classes={{ body: displayState.className}} align="center"
            onContextMenu={e => e.preventDefault()}
            onMouseDown={this.handleClicked}>
                {displayState.value}
        </TableCell>;
    }
}

export default Square;