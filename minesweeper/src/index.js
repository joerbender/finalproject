import React from 'react';
import ReactDOM from 'react-dom';
import App from './navigation/App';

ReactDOM.render(<App/>, document.getElementById('root'));