import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withRouter } from 'react-router-dom';
import Copyright from './Copyright';
import { isGoodEmail } from './SignInPage';
import Axios from 'axios';
import SiteHeader from './SiteHeader';

class SignUpPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      emailAddress: '',
      password: '',
      firstName: '',
      lastName: '',
      receivePromo: false,
      failureMessage: ''
    };

    //bind the state effecting functions to 'this' instance
    this.handleEmailAddressChanged = this.handleEmailAddressChanged.bind(this);
    this.handleTokenChanged = this.handleTokenChanged.bind(this);
    this.handleFirstNameChanged = this.handleFirstNameChanged.bind(this);
    this.handleLastNameChanged = this.handleLastNameChanged.bind(this);
    this.handleRecievePromoChange = this.handleRecievePromoChange.bind(this);
    this.handleSubmitted = this.handleSubmitted.bind(this);
    this.renderFailureMessage = this.renderFailureMessage.bind(this);
  }

  //handles the email address state when the text changes
  handleEmailAddressChanged(event) {
    this.setState({ emailAddress: event.target.value });
  }

  //handles the password state when it is changed
  handleTokenChanged(event) {
    this.setState({ password: event.target.value });
  }

  //handles the first name state when it is changed
  handleFirstNameChanged(event) {
    this.setState({ firstName: event.target.value });
  }

  //handles the last name state when it is changed
  handleLastNameChanged(event) {
    this.setState({ lastName: event.target.value });
  }

  //handles the state change of whetehr or not the user wants to receive promotions
  handleRecievePromoChange(event) {
    this.setState({ receivePromo: !this.state.receivePromo });
  }

  //renders the failure message if the user does not set up the account correctly
  renderFailureMessage() {
    const { classes } = this.props;
    const { failureMessage } = this.state;
    if (failureMessage.length > 0) {
      return (<Typography className={classes.errorMsg}>
        {failureMessage}
      </Typography>);
    }
    return <div></div>;
  }

  //handle when the account information is submitted by the user
  async handleSubmitted() {
    const { handleSignedIn, history } = this.props;
    const { emailAddress, password, firstName, lastName, receivePromo } = this.state;
    //check if the user left any fields blank
    if (emailAddress.length === 0 || password.length === 0 || firstName.length === 0 || lastName.length === 0) {
      //let the user know they forgot something.....
      this.setState({ failureMessage: 'Please fill in all required fields.' });
    } else if (isGoodEmail(emailAddress)) {
      //check if the user account already exists
      try {
        await Axios.head('/api/user/', {
          params: {
            emailAddress: emailAddress
          }
        });
        //let the user know that the information is already in use
        this.setState({ failureMessage: 'Email address is already used by another account' });
      } catch (error) {
        //create the user object and raise it to a higher state
        await Axios.post('/api/user', { 
              emailAddress: emailAddress, 
              password: password, 
              firstName: firstName, 
              lastName: lastName, 
              receivePromo: receivePromo });
              
        const { data : { user } } = await Axios.get('/api/user', {
          params: {
            emailAddress: emailAddress,
            password: password
          }
        });
        handleSignedIn(user);
        history.push('');
      }
    } else {
      //let the user know their email is not valid
      this.setState({ failureMessage: 'Invalid email address.' });
    }
  }

  render() {
    const { classes, history } = this.props;
    const { emailAddress, password, firstName, lastName } = this.state;
    return (
      <Container component="main" maxWidth="xs">
      ` <SiteHeader/>
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
              </Typography>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                {/* First name text field */}
                <TextField
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  value={firstName}
                  onChange={this.handleFirstNameChanged}
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                {/* Last name text field */}
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  value={lastName}
                  onChange={this.handleLastNameChanged}
                  autoComplete="lname"
                />
              </Grid>
              <Grid item xs={12}>
                {/* Email address text field */}
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  value={emailAddress}
                  onChange={this.handleEmailAddressChanged}
                  autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                {/* password text field */}
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  value={password}
                  onChange={this.handleTokenChanged}
                  autoComplete="current-password"
                />
              </Grid>

              <Grid item xs={12}>
                {/* Send user promo stuff check box */}
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="I want to receive inspiration, marketing promotions and updates via email."
                />
              </Grid>
            </Grid>
            {/* Submist credentials button */}
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.handleSubmitted}
            >
              Sign Up
                </Button>
            <Grid container justify="flex-end">
              <Grid item>
                {/* Link to send user to sign in page */}
                <Link onClick={() => history.push("/signIn")} variant="body2">
                  Already have an account? Sign in
                    </Link>
              </Grid>
            </Grid>
          </form>
        </div>

        {/* render any login failure messages to display to the user */}
        {this.renderFailureMessage()}
        <Box mt={5}>
          <Copyright />
        </Box>
      </Container>
    );
  }
}

//dynamically create the styles using the theme object for a universal look/feel
const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(-10),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  errorMsg: {
    color: 'red',
    margin: theme.spacing(3, 0, 2),
    alignItems: 'center'
  }
});

export default withStyles(styles)(withRouter(SignUpPage));