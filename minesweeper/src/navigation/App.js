import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SignInPage from './SignInPage';
import SignUpPage from './SignUpPage';
import Scoreboard from './Scoreboard';
import Game from './Game';

//main application hub for the site
class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      signedInUser: null
    }
    
    this.handleSignedIn = this.handleSignedIn.bind(this);
  }

  //handles when a user has signed in
  handleSignedIn(signedInUser) {
    this.setState({ signedInUser: signedInUser });
  }

  //renders the site
  render() {
    const { signedInUser } = this.state;
    return <Router>
      <div background-color="#7189ac">
        <Switch>
          <Route path="/scoreboard">
            <Scoreboard signedInUser={signedInUser} />
          </Route>
          <Route path="/signUp">
            <SignUpPage handleSignedIn={this.handleSignedIn} />
          </Route>
          <Route path="/signIn">
            <SignInPage handleSignedIn={this.handleSignedIn} />
          </Route>
          <Route>
            <Game signedInUser={signedInUser} />
          </Route>
        </Switch>
      </div>
    </Router>
  }
}

export default App;