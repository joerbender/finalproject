import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Copyright from './Copyright';
import { withRouter } from 'react-router-dom';
import Axios from 'axios';
import SiteHeader from './SiteHeader';

//Verifies email is good enough on the client side, exported so that it can be used elsewhere
export function isGoodEmail(emailAddress) {
  //could put more checks in here but just wan
  return emailAddress.includes('@');
}

class SignInPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      emailAddress: '',
      password: '',
      failureMessage: ''
    };

    //bind the state effecting functions
    this.handleSubmittedLogin = this.handleSubmittedLogin.bind(this);
    this.handleEmailAddressChanged = this.handleEmailAddressChanged.bind(this);
    this.handleTokenChanged = this.handleTokenChanged.bind(this);
    this.renderFailureMessage = this.renderFailureMessage.bind(this);
  }

  //handle when the user attempts to login
  async handleSubmittedLogin() {
    const { history, handleSignedIn } = this.props;
    const { emailAddress, password } = this.state;
    //check if any fields have not been filled out
    if (emailAddress.length === 0 || password.length === 0) {
      //tell the user they forgot something....
      this.setState({ failureMessage: 'Please fill in all required fields.' });
    } else if (isGoodEmail(emailAddress)) {
      // IRL end login to the server, presumabely for some return authorization
      // for our purposes just see if the user exists out of the current users and we'll call that good enough
      try {
        const response = await Axios.get('/api/user', {
          params: {
            emailAddress: emailAddress,
            password: password
        }});
        const { data : { user } } = response;
        if (user !== undefined) {
          //raise the state to add the user as the current signed in user for the session
          handleSignedIn(user);
          history.push('');
        } else {
          //tell the user that they have not created an account yet
          this.setState({ failureMessage: 'User does not exist. Please create an account to proceed!' });
        }
      } catch (error) {
        console.log(error);
        this.setState({ failureMessage: 'User does not exist. Please create an account to proceed!' });
      }

    } else {
      //tell the user their email is no bueno
      this.setState({ failureMessage: 'Invalid email address.' });
    }
  }

  //handles the state change when the text for email address changes
  handleEmailAddressChanged(event) {
    this.setState({ emailAddress: event.target.value });
  }

  //handles the state change when the password/pw text changes
  handleTokenChanged(event) {
    this.setState({ password: event.target.value });    
  }

  //renders a message to the user if they did not enter the fields correctly
  renderFailureMessage() {
    const { classes } = this.props;
    const { failureMessage } = this.state;
    if (failureMessage.length > 0) {
      return (<Typography className={classes.errorMsg}>
        { failureMessage }
      </Typography>);
    }
    return <div></div>;
  }

  //the main rendor call for the component
  render() {
    const { classes, history } = this.props;
    const { emailAddress, password } = this.state;
    return (<Container width="xs" maxWidth="xs" onSubmit={this.handleSubmittedLogin}>
        <SiteHeader/>
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            {/* Email Text Field Input */}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              value={emailAddress}
              onChange={this.handleEmailAddressChanged}
              autoComplete="email"
              autoFocus
            />
            {/* Password Field Input */}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              value={password}
              onChange={this.handleTokenChanged}
              autoComplete="current-password"
            />
            
            {/* Remember me check box */}
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />

            {/* Submit credentials button for sign in */}
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={this.handleSubmittedLogin}
            >
            Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                {/* Fogotton password link */}
                <Link variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                {/* Sign up link */}
                <Link onClick={() => history.push("/signUp")} 
                      component="button">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        {/* Login error messages */}
        {this.renderFailureMessage()}
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    );
  }
}


//dynamically create the styles using the theme object for a universal look/feel
const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.grey,
    },
  },
  paper: {
    marginTop: theme.spacing(-8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  //props for the error user error message
  errorMsg: {
    color: 'red',
    margin: theme.spacing(3, 0, 2),
    alignItems: 'center'
  }
});

//export the object with styles for dynamic rendoring and the router for pushes to the history (router interaction)
export default withStyles(styles)(withRouter(SignInPage));