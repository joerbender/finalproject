import React, { Component } from 'react';
import { Typography, Button, Container, Table, TableRow, TableHead, TableCell, TableBody, CssBaseline, Box } from '@material-ui/core';
import { withRouter } from 'react-router-dom'
import Axios from 'axios';
import Moment from 'react-moment';
import Copyright from './Copyright';
import SiteHeader from './SiteHeader';

const scoreColumns = ["Score",
                "User Name",
                "Height x Width",,
                "Mines",
                "Date"];

// represents the scoreboard page
class Scoreboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            scores:[]
        }

        this.renderScoreRow = this.renderScoreRow.bind(this);
    }

    //updates when the component is mpuonted to the document
    async componentDidMount() {
        // gets the scores from the database
        const { data: scores } = await Axios.get('/api/score');
        this.setState({ scores: scores });
    }

    //renders a score row in the table
    renderScoreRow(scoreObj) {
        const { score, 
                user: { emailAddress }, 
                boardWidth, 
                boardHeight, 
                mineCount, 
                date } = scoreObj;
        return <TableRow key={"" + emailAddress + date}>
            <TableCell key={'s'+score} align="center">{score}</TableCell>
            <TableCell key={'u'+emailAddress} align="center">{emailAddress}</TableCell>
            <TableCell key={`wh${boardWidth}x${boardHeight}`} align="center">{`${boardWidth}x${boardHeight}`}</TableCell>
            <TableCell key={'m' + mineCount} align="center">{mineCount}</TableCell>
            <TableCell key={date} align="center">
                <Moment format="MMMM DD YYYY">{date}</Moment>
            </TableCell>
        </TableRow>;
    }

    //renders the scoreboard page for the user
    render() {
        const { scores: scoreData } = this.state;
        return <div>
            <SiteHeader/>
            <CssBaseline />
            <Container>
                <Typography variant="h3">Scoreboard</Typography>
                <Box m={2}>
                    <Button variant="contained" color="primary" onClick={() => this.props.history.push('')}>Back To The Game</Button>
                </Box>
                <Table width='100%' overflow='auto'>
                    <TableHead>
                        <TableRow>
                            {scoreColumns.map(colName => <TableCell key={colName} align="center">{colName}</TableCell>)}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {scoreData.map(this.renderScoreRow)}
                    </TableBody>
                </Table>
            </Container>
            <Copyright/>
        </div>;
    }
}

export default withRouter(Scoreboard);