import React, { Component } from 'react';
import { Typography, AppBar, Box, Link } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

//resusable site header to be used atop each page
class SiteHeader extends Component {

    render() {
        return <AppBar position="absolute" color="primary">
            <Box m={2}>
                <Typography padding="1rem" align="left" variant="h4" onClick={() => this.props.history.push('')} >Minesweeper</Typography>
            </Box>
        </AppBar>
    }
}

export default withRouter(SiteHeader);