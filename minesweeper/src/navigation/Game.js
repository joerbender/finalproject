import React, { Component } from 'react';
import Minesweeper from '../game/Minesweeper';
import { Button, Divider, AppBar, Box, Typography } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import styles from '../game/game.module.css';
import Copyright from './Copyright';
import SiteHeader from './SiteHeader';

// represents the game page to be displayed to the user
class Game extends Component {

    constructor(props) {
        super(props);

        //set the initial state to be in easy mode
        this.state = {
            numRows: 8,
            numColumns: 8,
            numMines: 10
        }

        this.updateBoard = this.updateBoard.bind(this);
    }

    //handles when the board is updated by the user selecting a different board size/ game difficulty
    updateBoard(numColumns, numRows, numMines) {
        this.setState({numRows: numRows, numColumns: numColumns, numMines: numMines})
    }

    //renders the entire game page to the user
    render() {
        const { numColumns, numRows, numMines } = this.state;
        const { signedInUser } = this.props;
        return <div>
            <SiteHeader/>
            <Box m={2} align="right">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.props.history.push('/signIn')}>
                    Sign In
                </Button>
            </Box>
            <Box m={2}>
                <Button disabled={!signedInUser}
                    variant="contained"
                    color="primary"
                    onClick={() => this.props.history.push('/scoreboard')}>
                    {!signedInUser ? "Sign in to View High Scores!" : "View Scoreboard"}                    
                </Button>
            </Box>
            <Box >
                <Minesweeper key={`${numRows}${numColumns}${numMines}`}
                    className={styles.minsweeper}
                    width={numColumns}
                    height={numRows}
                    numMines={numMines}
                    signedInUser={signedInUser} />
            </Box>
            <Divider margin="1rem" />
            <Box m={2}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={event => this.updateBoard(8, 8, 10)}>
                    Easy
                </Button>
            </Box>
            <Box m={2}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={event => this.updateBoard(15, 13, 40)}>
                    Medium
                </Button>
            </Box>
            <Box m={2}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={event => this.updateBoard(30, 16, 99)}>
                    Hard
                </Button>
            </Box>
            <Copyright/>
        </div>
    }
}

export default withRouter(Game);