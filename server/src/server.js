const express = require('express');
const bcrypt = require('bcryptjs');
const User = require('./model/user');
const Score = require('./model/score');

const app = express();
app.use(express.json());

const path = require('path');
app.use(express.static(path.join(__dirname, '../public', 'build')));

const mongoose = require('mongoose');

// connects to the remote db
const connectionString = "mongodb+srv://jbend2:m76vKf3dERATDt@cluster0-txobl.mongodb.net/minesweeper?retryWrites=true&w=majority";
mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true }, 
        () => { console.log("Connected to Database") })
        .catch(error => console.log(`Failed to connect to DB: ${error}`));

//handles head request for quick test if user exists
app.head('/api/user', async (request, response) => {   
    try { 
        const { query: { emailAddress } } = request;
        //find user via email address
        const user = await User.findOne({ "emailAddress" : emailAddress });
        if (user) {
            console.log(`Found user on head request: ${user}`);
            return response.sendStatus(200);
        } else {
            console.log(`Head request found no user: ${request}`);
            return response.sendStatus(404);
        }
    } catch (error) {
        console.log(error);
    }
    return response.sendStatus(404);
});

//handles request to get user given login credentials
app.get('/api/user', async (request, response) => {    
    try {
        const { query: { emailAddress, password }  } = request;
        const user = await User.findOne({ "emailAddress" : emailAddress }, { _id : 0});
        // make sure the passwordDigest matches the users input
        if (user && await bcrypt.compare(password, user.passwordDigest)) {
            //clear the password digest field before passing back to the user
            user.passwordDigest = undefined; 
            console.log(`Found user: ${user}`);
            return response.status(200).send({user});
        } else {
            console.log(`User does not exist: ${request}`);
            return response.sendStatus(404);
        }
    } catch (error) {
        console.log(error);
    }
});

//handles adding a new user to the db
app.post('/api/user', async (request, response) => {
    try {
        const { body: { firstName, lastName, emailAddress, password } } = request;
        const passwordDigest = await bcrypt.hash(password, 10);
        const user = await User.create({ firstName: firstName, 
                                         lastName: lastName, 
                                         emailAddress: emailAddress, 
                                         passwordDigest : passwordDigest });
        if (user) {
            console.log(`Created user: ${user}`);
            return response.sendStatus(200);
        } else {
            console.log(`User not found`);
        }
    } catch (error) {
        console.log(error);
    }
    console.log(`Invalid request: ${request}`);
    return response.sendStatus(400);
});

//handles when a user is deleted from the db
app.delete('/api/user', async (request, response) => {
    try {
        const { body: { emailAddress } } = request;
        const users = await User.find({ "emailAddress" : emailAddress });
        if (users != undefined) {
            try {
                users.delete();
                console.log(`Deleted user: ${users}`);
                return response.sendStatus(200);
            } catch (error) {
                console.log(`Failed to save user: ${error}`);
            }
        }
    } catch (error) {
        console.log(error);
    }
    console.log(`User does not exist: ${request}`);
    return response.sendStatus(404);
});

// gets descending scores from the db
app.get('/api/score', async (request, response) => {
    try {
        const { query : { user = null } } = request;
        let searchCriteria;
        if (user) {
            const { emailAddress = null } = JSON.parse(user);
            searchCriteria = await User.findOne({ "emailAddress" : emailAddress });
        }
        const scores = await Score.find(searchCriteria ? { "user" : userObj } : {}, { _id : 0})
                                        .sort("score")
                                        .populate({path: "user", select: "-passwordDigest -_id"});
        if (scores) {
            console.log("Found user score(s): " + scores);
            return response.send(scores);
        }
    } catch (error) {
        console.log(error);
    }
    return response.sendStatus(404);
});

// handles a new score being posted to the db
app.post('/api/score', async (request, response) => {
    try {
        const { body: { user, 
                score, 
                boardWidth, 
                boardHeight,
                mineCount } } = request;
        const userObj = await User.findOne({ "emailAddress" : user.emailAddress });
        if (userObj) {
            const scoreObj = await Score.create({
                user: userObj,
                score: score,
                boardWidth: boardWidth,
                boardHeight: boardHeight,
                mineCount: mineCount,
                date: new Date()
            });
            console.log(`Successfully created post: ${scoreObj}`);
            return response.sendStatus(200);
        } else {
            console.log(`Invalid request: ${request}`);
        }
    } catch (error) {
        console.log(error);
    }
    return response.sendStatus(400);
});

//handles the deletion of score(s) for a given user
app.delete('/api/score', async (request, response) => {
    try {
        const { query: { user, date, score } } = request;
        const { emailAddress = null } = JSON.parse(user);
        const userObj = await User.findOne({ "emailAddress": emailAddress });
        if (userObj) {
            Score.deleteMany({ "user": userObj, "date": date, "score": score });
            console.log("Delete user score(s): " + scores);
            return response.send(scores);
        }
    } catch (error) {
        console.log(error);
    }
    return response.sendStatus(404);
});

//catch all for every other type of request that comes our way, basically just return the start page
app.get('/*', async (request, response) => {
    try {
        return response.sendFile(path.join(__dirname, '../public', 'build', 'index.html'));
    } catch (error) {
        console.log(error);
        return response.sendStatus(500);
    }
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Server has started on localhost:${port}`));