const mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectID;

const scoreSchema = new mongoose.Schema({
    user: { type: ObjectId, ref: "User" },
    score: Number,
    boardWidth: Number,
    boardHeight: Number,
    mineCount: Number,
    date: Date
});

const Score = mongoose.model('Score', scoreSchema);

module.exports = Score;