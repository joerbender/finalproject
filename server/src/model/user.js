const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    emailAddress: String,
    firstName: String,
    lastName: String,
    picture: { type: String, default: 'https://www.gannett-cdn.com/presto/2019/06/20/USAT/6e5ed706-a548-4d10-a3d7-b4edcf2c0558-bat.jpeg' }
,    passwordDigest: String
});

const User = mongoose.model('User', userSchema);

module.exports = User;